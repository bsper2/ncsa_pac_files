function FindProxyForURL(url, host) {

    ///////////////////////////////////////////////////////////
    // Define Proxies
    ///////////////////////////////////////////////////////////
    var ache_bastion_proxy     = "SOCKS 127.0.0.1:6025";    // ACHE Proxy   
    var cerberus_bastion_proxy = "SOCKS 127.0.0.1:6031";    // vsphere.ncsa.illinois.edu and netdot
    var public_linux_proxy     = "SOCKS 127.0.0.1:6030";    // public_linux proxy for campus nagios
    var ngale_bmcOLD_proxy     = "SOCKS 127.0.0.1:6032";    // ngale old BMC (non routed have to double jump ache-bastion > mgadm01) 172.30.4.0/23
    var holli_bmc_proxy        = "SOCKS 127.0.0.1:6033";    // holli BMC, don't think cerberus bastions can reach holli BMC over https (442) need to double jump from bastion->hli-adm01 (might be ACL also set on the BMC!)
    var delta_proxy            = "SOCKS 127.0.0.1:6034";    // I have not tested this
    
    
    ///////////////////////////////////////////////////////////
    // Proxies by subnet
    ///////////////////////////////////////////////////////////

    // Define subnets
    var subnets_via_ngale_bmcOLD_proxy = [ { "name": "ngale_bmcOLD_subnet" , "start": "172.30.4.0"  , "netmask": "255.255.254.0"   }, ];
    var subnets_via_holli_proxy =        [ { "name": "holli_subnet"        , "start": "172.28.40.0" , "netmask": "255.255.255.192" }, ];
    var subnets_via_delta_proxy =        [ { "name": "delta_ipmi"          , "start": "172.28.24.0" , "netmask": "255.255.254.0"   }, ];
    var subnets_via_cerberus_bastion   = [ { "name": "rad_phoenix_ipmi"    , "start": "172.28.50.0" , "netmask": "255.255.255.0"   }, 
                                           { "name": "rad_prod_ipmi"       , "start": "172.29.0.0"  , "netmask": "255.255.252.0"   },    // Radiant BMC
                                           { "name": "rad_test_ipmi"       , "start": "172.29.11.0" , "netmask": "255.255.255.0"   }, ]; // Radiant TEST BMC
    var subnets_via_ache_bastion       = [ { "name": "ache_facilities_mgmt", "start": "172.28.3.192", "netmask": "255.255.255.192" }, // ACHE PDUs, UPSs, other environmentals
                                           { "name": "neteng_mgmt_net"     , "start": "172.28.3.0"  , "netmask": "255.255.255.192" }, // NetEng mgmt net (Probably don't need)
                                           { "name": "ACHE_HyperVisor_mgmt", "start": "172.28.2.192", "netmask": "255.255.255.192" },
                                           { "name": "ACHE_IPMI_Net"       , "start": "172.28.2.64" , "netmask": "255.255.255.192" },
                                           { "name": "ngale_ipmi_net"      , "start": "172.28.28.0" , "netmask": "255.255.254.0"   },
                                           { "name": "ngale_FacilitiesMgmt", "start": "172.28.3.128", "netmask": "255.255.255.192" },
                                           { "name": "mForge_VMware_IPMI"  , "start": "172.28.2.0"  , "netmask": "255.255.255.192" },
                                           { "name": "mforge_ipmi"         , "start": "172.28.14.0" , "netmask": "255.255.254.0"   }, ];

    // cerberus_bastion_proxy
    for (var i = 0; i < subnets_via_cerberus_bastion.length; i++) {
        if ( isInNet(host, subnets_via_cerberus_bastion[i].start, subnets_via_cerberus_bastion[i].netmask) )
            return cerberus_bastion_proxy;
    }
    
    // ache_bastion_proxy
    for (var i = 0; i < subnets_via_ache_bastion.length; i++) {
        if ( isInNet(host, subnets_via_ache_bastion[i].start, subnets_via_ache_bastion[i].netmask) )
            return ache_bastion_proxy;
    }
    
    // ngale_bmcOLD_proxy
    for (var i = 0; i < subnets_via_ngale_bmcOLD_proxy.length; i++) {
        if ( isInNet(host, subnets_via_ngale_bmcOLD_proxy[i].start, subnets_via_ngale_bmcOLD_proxy[i].netmask) )
            return ngale_bmcOLD_proxy;
    }

    // holli_bmc_proxy
    for (var i = 0; i < subnets_via_holli_proxy.length; i++) {
        if ( isInNet(host, subnets_via_holli_proxy[i].start, subnets_via_holli_proxy[i].netmask) )
            return holli_bmc_proxy;
    }
    
    // delta_proxy
    for (var i = 0; i < subnets_via_delta_proxy.length; i++) {
        if ( isInNet(host, subnets_via_delta_proxy[i].start, subnets_via_delta_proxy[i].netmask) )
            return delta_proxy;
    }
    
    ///////////////////////////////////////////////////////////
    // Proxies by hostname
    ///////////////////////////////////////////////////////////
    
    // ACHE
    if (
        host == "ache-vcenter.internal.ncsa.edu" ||
        host == "mvdi01.internal.ncsa.edu" ||
        host == "mvdi02.internal.ncsa.edu" ||
        host == "mvdi03.internal.ncsa.edu" ||
        host == "mvdi04.internal.ncsa.edu" ||
        host == "mesx01.internal.ncsa.edu" ||
        host == "mesx02.internal.ncsa.edu" ||
        host == "mesx03.internal.ncsa.edu" ||
        host == "mesx04.internal.ncsa.edu" ||
        host == "ache-esx01.internal.ncsa.edu" ||
        host == "ache-esx02.internal.ncsa.edu" ||
        host == "ache-git.ncsa.illinois.edu" ||
        host == "ache-repo.ncsa.illinois.edu"
    ) return ache_bastion_proxy
    

    // vsphere.ncsa.illinois.edu, netdot, mforge ache firewall graphs, etc
    if (
        host == "vsphere.ncsa.illinois.edu" ||
        host == "netdot.ncsa.illinois.edu" ||
        host == "its-monitor.ncsa.illinois.edu" ||
        host == "grafana.ncsa.illinois.edu" ||
        host == "oncall-test.ncsa.illinois.edu" ||
        host == "vcenter.internal.ncsa.edu"
    ) return cerberus_bastion_proxy;

    // public_linux proxy for mforge ache firewall graphs, and campus nagios
    if (
        host == "mforgexdmod.internal.ncsa.edu" ||
        host == "internal.ncsa.illinois.edu" ||
        host == "crashplan.ncsa.illinois.edu" ||
        host == "identity.ncsa.illinois.edu" ||
        host == "jiracmdline.ncsa.illinois.edu" ||
        host == "viewalert02.techservices.illinois.edu" ||
        host == "viewalert01.techservices.illinois.edu"
    ) return public_linux_proxy
	
    ///////////////////////////////////////////////////////////
    // Else
    ///////////////////////////////////////////////////////////
    
    // No match
    return "DIRECT";
}

